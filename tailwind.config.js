/** @type {import('tailwindcss').Config} */
export default {
  content: [],
  theme: {
    extend: {
      fontSize: {
        'normal': '15px'
      },
      
      colors: {
        'primary': '#795DA8',
        'secondary': '#F7F2FB'
      }
    },
  },
  plugins: [],
}

