const id = {
    greeting: 'Halo',
    menus: {
        'home': 'Beranda',
        'about': 'Tentang Kami',
        'payment': 'Metode Pembayaran',
        'pricing': 'Harga',
        'contact': 'Hubungi Kami',
        'login': 'Masuk Klien',
        'request_demo': 'Minta demo',
        'contact_sales': 'Hubungi Penjualan',
        'register': 'Daftar',
        'support': 'Dukungan',
        'fees': 'Biaya'
    },
    home_detail: 'Perkenalkan Payku: Gateway Pembayaran Tercepat, Paling Mudah, dan Paling Aman untuk Pemilik Hotel dan Properti di Indonesia.',
    home_subdetail: `Payku adalah platform <b>pemrosesan pembayaran</b> komprehensif. Dengan integrasi tunggal, bisnis dapat dengan mudah menerima pembayaran menggunakan <b>kartu kredit dan debit, dompet elektronik, transfer bank</b>, dan metode pembayaran populer lainnya. Teknologi pemrosesan pembayaran canggih kami memastikan <b>pemrosesan pembayaran yang cepat dan aman</b>.
    <br/><br/>
    Selain itu, Payku memungkinkan pemilik hotel, pemilik sewa liburan untuk mengenakan <b>kartu kredit virtual</b> dari OTA populer seperti <b>Booking.com, Traveloka, Expedia, dan Agoda</b>. Platform berbasis web kami <b>mengubah setiap browser web menjadi terminal kartu kredit seluler</b>, membuatnya sempurna untuk pemilik <b>tanpa sistem POS</b> atau akun kartu kredit pedagang.`,
    heartbeat: 'Terima uang dengan cepat!',
    effortless: 'Terima pembayaran online dengan mudah menggunakan solusi kami, mulai dari sekarang!',
    onboarding: 'Daftar sekarang untuk proses masuk yang cepat dan mudah.',
    benefit_1: {
        title: 'Pembayaran Tautan',
        subtitle: 'Minta pembayaran dari pelanggan Anda melalui email, SMS, atau media sosial. Begitu diterima, pelanggan Anda dapat melakukan pembayaran dengan cepat dan aman.'
    },
    benefit_2: {
        title: 'Kartu Kredit Virtual',
        subtitle: 'Kartu virtual akan segera menjadi hal yang biasa dalam industri perjalanan. Payku memungkinkan pemilik hotel dan sewa liburan untuk mengenakan biaya kartu kredit virtual dari OTA populer seperti Booking.com, Traveloka, Expedia, dan Agoda.'
    },
    benefit_3: {
        title: 'Pembayaran QR',
        subtitle: 'Nikmati kemudahan Pembayaran QR, yang memungkinkan pelanggan Anda melakukan pembayaran di tempat menggunakan metode pembayaran pilihan mereka tanpa perlu terminal POS. Terima pembayaran tanpa infrastruktur. Pembayaran Tanpa Sentuh.'
    },
    benefit_4: {
        title: 'Integrasi',
        subtitle: 'Dengan Payku terintegrasi langsung ke mesin pemesanan mitra kami, pemrosesan pembayaran menjadi lebih efisien, menghemat waktu berharga Anda dan tamu Anda.'
    },
    highspeed: 'Kecepatan tinggi. Biaya rendah. Tanpa masalah.',
    inoneplace: 'Semua Pembayaran Anda dalam Satu Tempat',
    how_it_work: 'Bagaimana cara kerjanya?',
    few_steps: 'Hanya beberapa langkah untuk memulai',
    steps_3: 'Lebih mudah daripada yang Anda pikirkan. Ikuti 3 langkah mudah sederhana',
    step_1: {
        title: 'Daftar gratis',
        subtitle: 'cukup isi formulir kami dan verifikasi identitas Anda'
    },
    step_2: {
        title: 'verifikasi',
        subtitle: 'tim kami akan mengatur panggilan untuk konfirmasi'
    },
    step_3: {
        title: 'kredensial akun',
        subtitle: 'Anda akan menerima kredensial akun Anda'
    },
    step_4: {
        title: "Anda selesai!",
        subtitle: 'mulai melakukan transaksi langsung dengan Payku. Akses dashboard terpusat Anda.'
    },
    about_us: 'Tentang kami',
    welcome_to_payku: 'Selamat datang di PAYKU, gateway pembayaran utama yang berspesialisasi untuk industri hotel di Indonesia. Kami adalah produk dari perusahaan utama kami, <span class="text-black">BOOKANDLINK</span>, yang didedikasikan untuk menyediakan solusi perangkat lunak yang mulus dan canggih kepada hotel di seluruh dunia. Di PAYKU, kami memahami tantangan unik yang dihadapi oleh pemilik hotel dalam menangani transaksi dan memastikan pengalaman pembayaran tanpa masalah bagi tamu mereka.',
    about_1: 'Dukungan lokal yang didedikasikan',
    about_2: 'Membangun untuk pasar Indonesia',
    about_3: '+11k transaksi per hari',
    our_values: 'Nilai-nilai Kami',
    our_values_title: 'Nilai-nilai yang mendorong segala yang kami lakukan',
    our_mission: 'Misi Kami',
    our_mission_title: 'Di PAYKU, misi kami adalah memberdayakan hotel dengan gateway pembayaran yang handal dan efisien yang menyederhanakan proses pembayaran, meningkatkan kepuasan tamu, dan memaksimalkan potensi pendapatan. Kami berusaha menjadi mitra pembayaran pilihan untuk hotel, menawarkan rangkaian solusi pembayaran komprehensif yang disesuaikan dengan kebutuhan khusus mereka.',
    who_we_are: 'Siapa kami',
    who_we_are_title: 'PAYKU adalah tim profesional berpengalaman yang bersemangat tentang teknologi, perhotelan, dan memberikan solusi pembayaran yang luar biasa. Dengan pemahaman mendalam tentang industri hotel, kami memanfaatkan keahlian kami untuk mengembangkan layanan gateway pembayaran inovatif yang secara khusus ditujukan untuk hotel, memastikan mereka tetap unggul di lanskap digital yang terus berkembang.',
    what_we_offer: 'Apa yang kami tawarkan',
    what_we_offer_list: `
        <li>
            <strong>Pemrosesan Pembayaran yang Aman dan Terpercaya:</strong> Kami menyediakan platform pembayaran yang tangguh dan aman yang memungkinkan hotel untuk menerima pembayaran dengan lancar. Teknologi enkripsi canggih kami memastikan perlindungan data sensitif tamu, memberikan kedamaian pikiran baik kepada hotel maupun tamu selama transaksi.
        </li>
        <li>
            <strong>Beragam Pilihan Pembayaran:</strong> Mengakui pentingnya menawarkan berbagai pilihan pembayaran, PAYKU mendukung berbagai metode pembayaran seperti kartu kredit dan debit, dompet digital, perbankan online, dan lainnya. Fleksibilitas ini memungkinkan tamu untuk memilih metode pembayaran yang diinginkan, meningkatkan pengalaman mereka secara keseluruhan.
        </li>
        <li>
            <strong>Integrasi yang Lancar:</strong> Gateway pembayaran kami terintegrasi dengan mudah dengan sistem manajemen properti (PMS) terkemuka dan mesin pemesanan online, memungkinkan hotel untuk menyederhanakan operasi mereka dan mengotomatisasi proses pembayaran. Integrasi ini mengurangi upaya manual, mengurangi kesalahan, dan meningkatkan efisiensi operasional.
        </li>
        <li>
            <strong>Jangkauan Global:</strong> Kami memahami bahwa industri hotel beroperasi secara global. Itulah mengapa PAYKU memfasilitasi pembayaran internasional, memungkinkan hotel untuk melayani tamu dari seluruh dunia. Dengan dukungan multi-mata uang dan kemampuan transaksi lintas batas, hotel dapat memperluas basis pelanggan mereka dan meningkatkan pendapatan.
        </li>
        <li>
            <strong>Dukungan Terdedikasi:</strong> Tim ahli kami berkomitmen untuk memberikan dukungan pelanggan yang luar biasa. Baik Anda memiliki pertanyaan, membutuhkan bantuan teknis, atau memerlukan panduan untuk mengoptimalkan proses pembayaran Anda, kami siap membantu. Kami memberikan prioritas membangun hubungan yang kuat dengan klien kami, memastikan keberhasilan mereka menjadi hal utama dalam setiap yang kami lakukan.
        </li>
    `,
    why_choose_payku: 'Mengapa Memilih PAYKU',
    why_choose_payku_list: `
        <li>
            <strong>Keahlian Industri:</strong> Kami mengkhususkan diri dalam menyediakan solusi pembayaran untuk industri hotel, sehingga kami memahami kebutuhan dan tantangan unik yang dihadapi oleh para pemilik hotel.
        </li>
        <li>
            <strong>Peningkatan Pengalaman Tamu:</strong> Dengan menyediakan pengalaman pembayaran yang lancar dan aman, kami berkontribusi pada peningkatan kepuasan tamu dan pembentukan loyalitas.
        </li>
        <li>
            <strong>Peningkatan Pendapatan:</strong> Dengan solusi pembayaran komprehensif kami, hotel dapat mengoptimalkan potensi pendapatan mereka dan mendapatkan lebih banyak pemesanan dengan menawarkan berbagai pilihan pembayaran.
        </li>
        <li>
            <strong>Terpercaya dan Aman:</strong> Kami memberikan prioritas pada keamanan semua transaksi, dengan menerapkan langkah-langkah terkemuka dalam perlindungan data sensitif dan memastikan kepatuhan terhadap regulasi perlindungan data.
        </li>
    `,
    join_us: 'Bergabunglah dengan kami di PAYKU dan rasakan kekuatan gateway pembayaran yang dirancang khusus untuk industri hotel. Simplifikasi proses pembayaran Anda, tingkatkan kepuasan tamu, dan maksimalkan potensi pendapatan dengan solusi pembayaran komprehensif kami. Bersama-sama, kita dapat membentuk masa depan pembayaran di sektor perhotelan.',
    payment_methods: 'Metode Pembayaran',
    payment_methods_compatible: 'Kompatibel dengan metode pembayaran terkemuka',
    payment_methods_compatible_desc: 'Perluas bisnis Anda di Indonesia, Filipina, dan Asia Tenggara dengan jaringan mitra kami yang luas dan lebih dari 140+ bank lokal. Terima notifikasi waktu nyata ketika pembayaran telah dilakukan. Mudah rekonsiliasi transaksi dan proses pengembalian dana melalui dashboard payku.',
    choose_country: 'Pilih negara',
    pricing: 'Harga',
    pricing_simple: 'Harga yang sederhana dan transparan',
    pricing_desc: 'Disesuaikan untuk bisnis dan organisasi dari semua ukuran. Mulai dengan mudah bahkan jika Anda tidak memiliki situs web, pilih untuk mengintegrasikan dengan plugin populer atau melalui integrasi API. Payku menawarkan harga yang adil dan transparan, tanpa biaya tambahan untuk integrasi API.',
    pricing_desc_italic: 'Semua harga tidak termasuk PPN, kecuali untuk QRIS dan ShopeePay.',
    payments: 'Pembayaran',
    payments_methods_easily: 'Terima metode pembayaran utama dengan mudah',
    payments_methods_easily_desc: 'Dapatkan akses ke berbagai pilihan pembayaran. Transfer bank, kartu kredit dan debit, outlet ritel, rencana angsuran, dan dompet elektronik melalui integrasi tunggal dengan Payku. Opsi penyelesaian awal juga tersedia, sehingga Anda dapat menerima dana Anda lebih awal.',
    transacting_500m: 'Bertransaksi Rp 500 juta atau lebih per bulan? Hubungi kami untuk dukungan personalisasi hari ini.',
    copyright: 'Hak Cipta',
    powered_by: 'Dipersembahkan Oleh',
    per_transaction: 'Per Transaksi',
    transaction_fee: 'Biaya transaksi adalah 2,9%. Selain itu, untuk transaksi kurang atau sama dengan Rp 400.000, akan dikenakan biaya layanan Rp 2.000.',
    payment_methods_data: [
        {
            "payment_method_name_short": "Virtual Accounts",
            "payment_method_name_long": "Virtual Accounts (Transfer Bank)",
            "description": "Mengenali dan menerima pembayaran dengan mudah melalui berbagai bank dengan satu Virtual Account (VA), tanpa harus mempertahankan beberapa rekening bank terpisah.",
            "image": "https://paykupayments.com/wp-content/uploads/2023/05/Screenshot_42-1024x315.png"
        },
        {
            "payment_method_name_short": "Kartu",
            "payment_method_name_long": "Kartu",
            "description": "Kami mendukung pembayaran lokal dan internasional untuk kartu kredit/debit Visa, MasterCard, JCB, dan AMEX. Pedagang yang memproses pembayaran kartu menikmati tingkat penerimaan hingga 30% lebih tinggi dengan menggunakan Xendit, sambil menjaga tingkat pengembalian uang rendah menggunakan sistem deteksi penipuan internal kami.",
            "image": "https://paykupayments.com/wp-content/uploads/2023/05/JCB-logo.png"
        },
        {
            "payment_method_name_short": "Outlet Ritel / OTC",
            "payment_method_name_long": "Outlet Ritel / Over-the-counter",
            "description": "Pelanggan Anda dapat dengan mudah membayar dengan tunai di lebih dari 12.000 gerai convenience store, termasuk Alfamart, Alfamidi, Alfaexpress, dan Dan+Dan.",
            "image": "https://paykupayments.com/wp-content/uploads/2023/05/Screenshot_43-768x190.png"
        },
        {
            "payment_method_name_short": "E-wallet",
            "payment_method_name_long": "Dompet Elektronik",
            "description": "Integrasi kami dengan e-wallet terkemuka di Indonesia memungkinkan pelanggan Anda membayar dengan mudah. Untuk OVO, DANA, LinkAja, dan ShopeePay, Anda juga dapat mengaktifkan API e-wallet dan meningkatkan tingkat keberhasilan pembayaran dengan menggunakan tokenisasi.",
            "image": "https://paykupayments.com/wp-content/uploads/2023/05/Screenshot_44.png"
        },
        {
            "payment_method_name_short": "Kode QR",
            "payment_method_name_long": "Kode QR dengan QRIS",
            "description": "Terima pembayaran dengan mudah melalui kode QR di toko fisik atau online. Integrasi kami dengan QRIS sekarang memungkinkan pelanggan Anda membayar dengan lancar dari e-wallet terkemuka (OVO, Gopay, DANA, LinkAja, ShopeePay) dan aplikasi perbankan seluler terkemuka (BCA, CIMB).",
            "image": null
        },
        {
            "payment_method_name_short": "Debit Langsung",
            "payment_method_name_long": "Debit Langsung",
            "description": "Terima pembayaran dengan mudah dengan menarik dana langsung dari rekening bank pelanggan Anda. Pelanggan Anda juga dapat menyimpan rincian rekening bank mereka untuk membayar transaksi di masa mendatang dengan mudah.",
            "image": "https://paykupayments.com/wp-content/uploads/2023/05/Screenshot_45.png"
        },
        {
            "payment_method_name_short": "Bayar Nanti",
            "payment_method_name_long": "Bayar Nanti",
            "description": "Biarkan pelanggan Anda membeli sekarang dan bayar nanti dalam 30 hari atau dalam cicilan. Pelanggan dengan garis kredit digital aktif dari salah satu mitra pemberi pinjaman kami dapat membayar melalui pinjaman cicilan yang fleksibel.",
            "image": "https://paykupayments.com/wp-content/uploads/2023/05/Screenshot_46.png"
        }
    ],
    "simple_integration": 'Integrasi Mudah',
    "real_time_notif": 'Notifikasi Real-time',
    "report_n_analytic": 'Analitik dan Pelaporan',
    'your_name': 'Nama Anda',
    your_email: 'Email Anda',
    your_message: 'Pesan Anda (Opsional)',
    phone_number: 'Nomor Telepon',
    hotel_name: 'Nama Hotel',
    date: 'Tanggal',
    time: 'Jam'
};

export default id;
