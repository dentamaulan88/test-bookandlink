const en = {
    greeting: 'Hello',
    menus: {
        'home': 'Home',
        'about': 'About Us',
        'payment': 'Payment Method',
        'pricing': 'Pricing',
        'contact': 'Contact Us',
        'login': 'Client Login',
        'request_demo': 'Request a demo',
        'contact_sales': 'Contact Sales',
        'register': 'Register',
        'support': 'Support',
        'fees': 'Fees'
    },
    home_detail: 'Introducing Payku: The Fastest, Easiest, and Most Secure Payment Gateway for Hoteliers and Property Owners in Indonesia.',
    home_subdetail: `Payku is a comprehensive <b>payment processing platform</b>. With a single integration, businesses can easily accept payments using <b>credit and debit cards, e-wallets, bank transfers</b>, and other popular payment methods. Our advanced payment processing technology ensures fast and <b>secure payment processing</b>.
    <br/><br/>
    Additionally, Payku enables hoteliers, vacation rental owners to charge <b>virtual credit cards</b> from popular OTAs like <b>Booking.com, Traveloka, Expedia, and Agoda</b>. Our web-based platform <b>turns any web browser into a mobile credit card terminal</b>, making it perfect for owners <b>without a POS system</b> or merchant credit card account.`,
    heartbeat: 'Receive money in a heartbeat!',
    effortless: 'Accept online payments effortlessly with our solutions, starting today!',
    onboarding: 'Register now for fast and easy onboarding.',
    benefit_1: {
        title: 'Link Payments',
        subtitle: 'Request payments from your customers via email, SMS, or social media. Once received, your customers can quickly and securely make payments.'
    },
    benefit_2: {
        title: 'Virtual Credit Cards',
        subtitle: 'Virtual cards will soon become the new normal in the travel industry. Payku enables hoteliers, vacation rental owners to charge virtual credit cards from popular OTAs like Booking.com, Traveloka, Expedia, and Agoda.'
    },
    benefit_3: {
        title: 'QR Pay',
        subtitle: 'Experience the convenience of QR Pay, which allows your customers to make on-site payments using their preferred payment methods without the need for a POS terminal. Receive payments without infrastructure. Contactless Payments.'
    },
    benefit_4: {
        title: 'Integration',
        subtitle: 'With Payku integrated directly into our booking engine partner, payment processing is streamlined, saving you and your guests valuable time'
    },
    highspeed: 'High speeds. Low fees. No hassle.',
    inoneplace: 'All Your Payments In One Place',
    how_it_work: 'How it works?',
    few_steps: 'Just few steps to start',
    steps_3: 'It’s easier than you think. Follow 3 simple easy steps',
    step_1: {
        title: 'Register for free',
        subtitle: 'simply fill up our form and verify your identity'
    },
    step_2: {
        title: 'verification',
        subtitle: 'our team will set up a call for confirmation'
    },
    step_3: {
        title: 'account credentials',
        subtitle: 'you will receive your account credentials'
    },
    step_4: {
        title: "You're all done!",
        subtitle: 'start making live transactions with Payku. Access your centralized dashboard.'
    },
    about_us: 'About us',
    welcome_to_payku: 'Welcome to PAYKU, the premier payment gateway specialized for the hotel industry in Indonesia. We are a product of our main company, <span class="text-black">BOOKANDLINK</span>, dedicated to providing seamless and advanced software solutions to hotels around the world. At PAYKU, we understand the unique challenges faced by hoteliers when it comes to handling transactions and ensuring a hassle-free payment experience for their guests.',
    about_1: 'Dedicated local support',
    about_2: 'Building for Indonesian market',
    about_3: '+11k transactions per day',
    our_values: 'Our values',
    our_values_title: 'The values that drive everything we do',
    our_mission: 'Our Mission',
    our_mission_title: 'At PAYKU, our mission is to empower hotels with a reliable and efficient payment gateway that simplifies the payment process, enhances guest satisfaction, and maximizes revenue potential. We strive to be the preferred payment partner for hotels, offering a comprehensive suite of payment solutions tailored to their specific needs.',
    who_we_are: 'Who we are',
    who_we_are_title: 'PAYKU is a team of experienced professionals who are passionate about technology, hospitality, and delivering exceptional payment solutions. With a deep understanding of the hotel industry, we leverage our expertise to develop innovative payment gateway services that cater specifically to hotels, ensuring they stay ahead in an ever-evolving digital landscape.',
    what_we_offer: 'What we offer',
    why_choose_payku_list: `
        <li>
            <strong>Industry Expertise:</strong> We specialize in providing payment solutions for the hotel industry, meaning we understand the unique requirements and challenges faced by hoteliers.
        </li>
        <li>
            <strong>Enhanced Guest Experience:</strong> By offering a smooth and secure payment experience, we contribute to enhancing guest satisfaction and fostering loyalty.
        </li>
        <li>
            <strong>Increased Revenue:</strong> With our comprehensive payment solutions, hotels can optimize their revenue potential and capture more bookings by offering a wide range of payment options.
        </li>
        <li>
            <strong>Reliable and Secure:</strong> We prioritize the security of all transactions, employing industry-leading measures to protect sensitive data and ensure compliance with data protection regulations.
        </li>
    `,
    why_choose_payku: 'Why Choose PAYKU',
    what_we_offer_list: `
        <li>
            <strong>Secure and Reliable Payment Processing:</strong> We provide a robust and secure payment platform that enables hotels to accept payments seamlessly. Our advanced encryption technology ensures the protection of sensitive guest data, giving both hotels and guests peace of mind during transactions.
        </li>
        <li>
            <strong>Multiple Payment Options:</strong> Recognizing the importance of offering a diverse range of payment options, PAYKU supports various payment methods such as credit and debit cards, mobile wallets, online banking, and more. This flexibility allows guests to choose their preferred payment method, improving their overall experience.
        </li>
        <li>
            <strong>Seamless Integration:</strong> Our payment gateway integrates effortlessly with leading property management systems (PMS) and online booking engines, allowing hotels to streamline their operations and automate payment processes. This integration minimizes manual effort, reduces errors, and enhances operational efficiency.
        </li>
        <li>
            <strong>Global Reach:</strong> We understand that the hotel industry operates on a global scale. That's why PAYKU facilitates international payments, enabling hotels to cater to guests from around the world. With multi-currency support and cross-border transaction capabilities, hotels can expand their customer base and boost revenue.
        </li>
        <li>
            <strong>Dedicated Support:</strong> Our team of experts is committed to providing exceptional customer support. Whether you have a question, need technical assistance, or require guidance on optimizing your payment processes, we are here to help. We prioritize building strong relationships with our clients, ensuring their success is at the forefront of everything we do.
        </li>
    `,
    join_us: 'Join us at PAYKU and experience the power of a payment gateway designed exclusively for the hotel industry. Simplify your payment processes, elevate guest satisfaction, and maximize revenue potential with our comprehensive payment solutions. Together, we can shape the future of payments in the hospitality sector.',
    payment_methods: 'Payment Methods',
    payment_methods_compatible: 'Compatible with leading payment methods',
    payment_methods_compatible_desc: 'Scale your business across Indonesia, the Philippines and Southeast Asia with our extensive network of partners and more than 140+ local banks. Receive real-time notifications when a payment has been made. Easily reconcile transactions and process refunds through the payku dashboard.',
    choose_country: 'Choose a country',
    pricing: 'Pricing',
    pricing_simple: 'Simple, transparent pricing',
    pricing_desc: 'Tailored to businesses and organisations of all sizes. Get started easily even if you don’t have a website, choose to integrate with popular plugins or via API integration. Payku offers fair and transparent pricing, with no additional charges for API integration.',
    pricing_desc_italic: 'All prices are exclusive of VAT, except for QRIS and ShopeePay.',
    payments: 'Payments',
    payments_methods_easily: 'Accept major payment methods easily',
    payments_methods_easily_desc: 'Gain access to a wide range of payment options. Bank transfers, credit and debit cards, retail outlets, installment plans and e-Wallets through a single integration with Payku. Early settlement options are also available, so you can receive your funds earlier.',
    transacting_500m: 'Transacting Rp 500m or more per month? Contact us for personalized support today.',
    copyright: 'Copyright',
    powered_by: 'Powered By',
    per_transaction: 'Per Transaction',
    transaction_fee: 'Transaction fees are 2.9%. Additionally, for transactions less or equal to 400,000 IDR, a 2,000 IDR service fee will apply.',
    payment_methods_data: [
        {
            "payment_method_name_short": "Virtual Accounts",
            "payment_method_name_long": "Virtual Accounts (Bank Transfer)",
            "description": "Automatically recognize and accept payments with ease across different banks with a single VA, without maintaining multiple separate bank accounts.",
            "image": "https://paykupayments.com/wp-content/uploads/2023/05/Screenshot_42-1024x315.png"
        },
        {
            "payment_method_name_short": "Cards",
            "payment_method_name_long": "Cards",
            "description": "We support local and international payments for credit / debit cards on Visa, MasterCard, JCB and AMEX. Merchants who process card payments enjoy up to 30% increased acceptance rates by using Xendit, while keeping chargeback rates low using our in-house fraud detection system.",
            "image": "https://paykupayments.com/wp-content/uploads/2023/05/JCB-logo.png"
        },
        {
            "payment_method_name_short": "Retail Outlets / OTC",
            "payment_method_name_long": "Retail Outlets / Over-the-counter",
            "description": "Your customers can easily pay with cash over the counter at 12,000+ convenience stores, including Alfamart, Alfamidi, Alfaexpress and Dan+Dan.",
            "image": "https://paykupayments.com/wp-content/uploads/2023/05/Screenshot_43-768x190.png"
        },
        {
            "payment_method_name_short": "E-wallets",
            "payment_method_name_long": "E-wallets",
            "description": "Our integrations with leading e-wallets in Indonesia allow your customers to pay easily. For OVO, DANA, LinkAja, and ShopeePay, you can also activate e-wallet API and maximize payment success rate by using tokenization.",
            "image": "https://paykupayments.com/wp-content/uploads/2023/05/Screenshot_44.png"
        },
        {
            "payment_method_name_short": "QR Codes",
            "payment_method_name_long": "QR Codes with QRIS",
            "description": "Accept payments easily through QR codes in physical or online stores. Our integration with QRIS now allows your customers to pay seamlessly from top e-wallets (OVO, Gopay, DANA, LinkAja, ShopeePay) and top mobile banking applications (BCA, CIMB).",
            "image": null
        },
        {
            "payment_method_name_short": "Direct Debit",
            "payment_method_name_long": "Direct Debit",
            "description": "Accept payments easily by pulling funds directly from your customer’s bank account. Your customers can also save their bank account details to pay future transactions conveniently.",
            "image": "https://paykupayments.com/wp-content/uploads/2023/05/Screenshot_45.png"
        },
        {
            "payment_method_name_short": "PayLater",
            "payment_method_name_long": "PayLater",
            "description": "Allow your customers to buy now and pay later in 30 days or in installments. Customers with an active digital credit line with one of our partner lenders can pay via flexible installment loans.",
            "image": "https://paykupayments.com/wp-content/uploads/2023/05/Screenshot_46.png"
        }
    ],
    "simple_integration": 'Simple Integration',
    "real_time_notif": 'Real time notification',
    "report_n_analytic": 'Reporting and Analytics',
    'your_name': 'Your Name',
    'your_email': 'Your Email',
    'your_message': 'Your Message (Optional)',
    'phone_number': 'Phone Number',
    'hotel_name': 'Hotel Name',
    'date': 'Date',
    'time': 'Time'
}

export default en