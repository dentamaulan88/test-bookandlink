// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  ssr: true,
  router: {
    linkExactActiveClass: 'text-secondary'
  },
  css: ['~/assets/css/global.css'],
  modules: ["@nuxtjs/i18n", "@nuxtjs/tailwindcss", "@nuxtjs/google-fonts", "nuxt-aos", "nuxt-icon"],
  googleFonts: {
    families: {
      Jost: [300, 400, 500, 600, 700]
    }
  },
  aos: {
    delay: 200,
    duration: 800
  },
  i18n: {
    locales: ['id', 'en'],
    strategy: 'prefix',
    defaultLocale: 'en',
    vueI8n: './i18n.config.ts'
  }
})