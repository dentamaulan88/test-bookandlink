import en from "./locales/en"
import id from "./locales/id"

export default defineI18nConfig(() => ({
    legacy: false,
    locale: 'en',
    fallbackLocale: 'en',
    messages: {
      en,id
    },
    warnHtmlInMessage: false
  }))
  